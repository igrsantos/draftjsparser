#!/usr/bin/env python
# coding: utf-8
import logging
import re
import sys

import simplejson as json
from bs4 import BeautifulSoup

from rules import apply_styles, clean_text, generate_key, is_text


class Block(object):

    INLINE_STYLES = [
        {'tag': 'b', 'style': 'BOLD'},
        {'tag': 'i', 'style': 'ITALIC'},
        {'tag': 'ins', 'style': 'UNDERLINE'}
    ]

    def __init__(self, child, entity_map, **kwargs):
        self.root = child
        self.entity_map = entity_map
        self.extra_inline_style = kwargs.get('style')
        # Default values
        self.key = generate_key()
        self.type = kwargs.get('type') or "unstyled"
        self.depth = 0
        self.data = {}

    def parse_inline_style(self, attr_style):
        # Fix content and get only inline styles permitted
        permitted_styles = ['color', 'font-family', 'font-size', 'text-align']
        return apply_styles([
            style.strip() for style in attr_style.split(';')
            if style.split(':')[0].strip() in permitted_styles
        ])

    def get_custom_style(self, child):
        styles = self.parse_inline_style(child.attrs['style'])
        child_text = clean_text(child)
        for style in styles:
            yield {
                'offset': self.text.find(child_text),
                'length': len(child_text),
                'style': style
            }

    @property
    def inlineStylesRanges(self):
        inlines = []
        if not is_text(self.root):
            if self.type != 'atomic':
                # Search common inline styles
                for inline in Block.INLINE_STYLES:
                    # Check style root
                    if self.root.name == inline['tag']:
                        inlines.append({
                            'offset': 0,
                            'length': len(self.text),
                            'style': inline['style']
                        })
                    # Check children style
                    for child in self.root.find_all(inline['tag']):
                        child_text = clean_text(child)
                        inlines.append({
                            'offset': self.text.find(child_text),
                            'length': len(child_text),
                            'style': inline['style']
                        })

            if self.root.attrs.get('style'):
                # Custom style to root
                styles = self.parse_inline_style(self.root.attrs['style'])
                for style in styles:
                    inlines.append({
                        'offset': 0,
                        'length': len(clean_text(self.root)),
                        'style': style
                    })

            # Search custom inline styles
            for child in self.root.select("[style]"):
                inlines.extend(self.get_custom_style(child))

        # Add extra style
        if self.extra_inline_style:
            styles = self.parse_inline_style(self.extra_inline_style)
            for style in styles:
                inline_style_range = {
                    'offset': 0,
                    'length': len(self.text),
                    'style': style
                }

                inlines.append(inline_style_range)

        return inlines

    @property
    def entityRanges(self):
        entity_ranges = []
        if not is_text(self.root):
            if self.type != 'atomic':
                # get all links to block
                for link in self.root.find_all('a'):
                    # Add content to entity map
                    data = {
                        'href': link.attrs.get('href'),
                        'target': link.attrs.get('target')
                    }
                    key = self.entity_map.add('LINK', EntityMap.MUTABLE, data)
                    entity_ranges.append({
                        'offset': self.text.find(clean_text(link)),
                        'length': len(clean_text(link)),
                        'key': key
                    })
            else:
                # get image data
                data = {
                    'src': self.root.attrs['src'],
                    'width': self.root.attrs.get('width'),
                    'heigth': self.root.attrs.get('heigth'),
                    'alt': self.root.attrs.get('alt')
                }
                key = self.entity_map.add('image', EntityMap.IMMUTABLE, data)
                entity_ranges.append({
                    'offset': 0,
                    'length': len(self.text),
                    'key': key
                })

        return entity_ranges

    @property
    def text(self):
        # Clean text remove break-line
        if (self.type != 'atomic'):
            return clean_text(self.root)
        else:
            return ' '

    @property
    def __dict__(self):
        # Override to return only attrs
        return {
            # "_root": self.root,
            "key": self.key,
            "text": self.text,
            "type": self.type,
            "depth": self.depth,
            "inlineStyleRanges": self.inlineStylesRanges,
            "entityRanges": self.entityRanges,
            "data": self.data
        }


class EntityMap(object):

    MUTABLE = 'MUTABLE'
    IMMUTABLE = 'IMMUTABLE'

    def __init__(self):
        self.entity_map = {}

    def add(self, entity_type, mutability, data):
        next_key = len(self.entity_map.keys())
        self.entity_map[str(next_key)] = {
            'type': entity_type,
            'mutability': mutability,
            'data': data
        }
        return next_key

    @property
    def __dict__(self):
        return self.entity_map


class Parser(object):

    BLOCK_TYPE = {
        'h1': 'header-one',
        'h2': 'header-two',
        'h3': 'header-three',
        'h4': 'header-four',
        'h5': 'header-five',
        'h6': 'header-six'
    }

    @classmethod
    def parse(cls, root, entity_map, **kwargs):
        # Remove childs like break-line
        children = [child for child in root.contents if child != '\n']

        for child in children:
            try:
                # Parse headers
                if 'class' in child.attrs.keys():
                    p = re.compile("wysiwyg-font-size-h{1}\d")
                    headers = list(filter(p.match, child.attrs['class']))
                    if headers:
                        element = headers[-1].split('-')[-1]
                        yield Block(child,
                                    entity_map,
                                    type=Parser.BLOCK_TYPE[element],
                                    **kwargs).__dict__

                if child.tag in Parser.BLOCK_TYPE.keys():
                    yield Block(child,
                                entity_map,
                                type=Parser.BLOCK_TYPE[child.tag],
                                **kwargs).__dict__

                # When br exists between children, should render blocks to
                # children with style of root
                if len(child.find_all('br')) > 0:
                    yield from Parser.parse(child,
                                            entity_map,
                                            style=child.attrs.get('style'))

                elif(child.name == 'img'):
                    yield Block(child,
                                entity_map,
                                type='atomic',
                                **kwargs).__dict__

                elif(child.name == 'ul'):
                    yield from Parser.parse(child,
                                            entity_map,
                                            type='unordered-list-item')

                elif(child.name == 'ol'):
                    yield from Parser.parse(child,
                                            entity_map,
                                            type='ordered-list-item')

                else:
                    yield Block(child, entity_map, **kwargs).__dict__
            except AttributeError as err:
                if is_text(child):
                    # If element is text create block unstyled with inherit
                    yield Block(child, entity_map, **kwargs).__dict__
                else:
                    logging.error('%s: %s', err, child)


def main(filename=None, html=None):
    soup = BeautifulSoup(html or open(filename), 'lxml')

    # entity_map should be mutable,
    # to store data all the blocks
    entity_map = EntityMap()
    blocks = Parser.parse(soup, entity_map)

    raw_content = {
        "entityMap": entity_map.__dict__,
        "blocks": list(blocks),
    }

    return json.dumps(raw_content,
                      indent=2,
                      ensure_ascii=False,
                      encoding='utf8')


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print('Incorrect command. Ex.: draftjsHtmlToContent.py [filename]')
    else:
        print(main(sys.argv[-1]))
