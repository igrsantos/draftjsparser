#!/usr/bin/env python
# coding: utf-8
import sys
import collections
import logging
import psycopg2
import psycopg2.extras


from draftjsHtmlToContent import main as parse


SELECT_QUERY = """
select m.slug, hstore_to_matrix(w.settings::hstore), w.id as widget_id
from mobilizations m
left join blocks b on m.id = b.mobilization_id
left join widgets w on b.id = w.block_id
where  w.updated_at < '2016-10-04 00:00:00'
and w.kind = 'content'
and w.settings is not null order by w.updated_at desc
"""

UPDATE_QUERY = """
update widgets
set settings = %(settings)s
where id = %(id)s
"""


def main(uri):
    logging.basicConfig(filename='update.log',
                        format='%(levelname)s %(asctime)s: %(message)s',
                        level=logging.DEBUG)

    # conn = psycopg2.connect("dbname=hub_api_development user=postgres")
    conn = psycopg2.connect(uri)
    psycopg2.extras.register_hstore(conn)
    cursor = conn.cursor()

    cursor.execute(SELECT_QUERY)
    for row in cursor.fetchall():
        logging.info('Parsing [%s] %s', row[2], row[0])
        # Update settings
        settings = collections.OrderedDict(row[1])
        settings.update({'content': parse(html=settings.get('content'))})
        cursor.execute(UPDATE_QUERY, {'settings': settings, 'id': row[2]})

    conn.commit()
    cursor.close()

if __name__ == '__main__':
    if (len(sys.argv) == 1):
        print('Cannot execute script without URI database. Ex.: ./update.py [uri]')
    else:
        main(sys.argv[-1])
