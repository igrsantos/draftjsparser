# coding: utf-8
import re
import random
from bs4.element import NavigableString


def is_text(child):
    if type(child) is str or type(child) is NavigableString:
        return True
    return False


def clean_text(child):
    """
    Remove extra whitespaces and line-breaks and change
    escape double quote to work JSON.parse()
    """
    if is_text(child):
        return child.replace('\"', '\\\"').replace('\n', '').strip()

    return child.get_text().replace('\"', '\\\"').replace('\n', '').strip()


def apply_styles(styles):
    """
    Apply rules to make styles
    apply_styles(styles:List): List
    """
    checked_style = []
    for style in styles:
        if 'text-align' in style:
            # text-align required only value
            # Example: text-align: center; > center
            ta_style = style.split(':')[-1].replace(';', '').strip()
            checked_style.append(ta_style)
        elif 'font-size' in style:
            # font-size required integer
            # Example: font-size: 13.333px; > font-size: 13px
            fs_style = style
            p = re.compile("\d+\.\d+")
            for px in p.findall(style):
                fs_style = style.replace(px, str(int(float(px))))
            checked_style.append(fs_style)
        else:
            checked_style.append(style)
    return checked_style


MULTIPLIER = 2**24

seed = []


def generate_key():
    key = None
    while not key or key in seed:
        key = hex(random.randint(0, MULTIPLIER)).lstrip('0x')
    seed.append(key)
    return key
