Draft-JS Parser
===============

Parser HTML created by WSYGI Editor used in [http://reboo.org](Reboo) to RawDraftContentState of [https://github.com/facebook/draft-js](draft-js)

# Install and run

Use Python 3:

`pip install requirements.txt`

To print console:

`./draftjsHtmlToContent.py [filename HTML]`

To save json file:

`./draftjsHtmlToContent.py [filename HTML] > [filename JSON]`

# TODO:

- [X] Remove break-line and extra whitespace in all text
- [X] Style block and children to text-align
- [X] Inline style to block like bold, text-align, text-size, font-family
- [X] Apply get style root in children of block
- [X] Fix encoding
- [X] Inline style to italic and underline (? indentify elements)
- [X] Change class wsygi-editor to inline style
- [X] Create block for element image
- [X] Create block for element list like ul, ol
- [X] Get data to link and make EntityRange
- [X] Generate auto key
